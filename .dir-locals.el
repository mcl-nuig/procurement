;;; Directory Local Variables
;;; For more information see (info "(emacs) Directory Variables")

((org-mode . ((org-use-sub-superscripts . "{}")
              (eval . (setq org-attach-id-dir
                            (concat
                             (magit-toplevel)
                             ".attachments"))))))
