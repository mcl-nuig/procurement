## Requisition Statement
<!-- Who is making the requisition and why? -->

- Required by:
- Project title:
- Purpose summary:

## Sub-account and budget holder
<!-- What is the subacc to be used?  Who's the subacc budget holder? -->

- Subacc:
- Budget holder:

## Items list
<!-- Complete list of items, their CAT number, the supplier name and ID, and cost (pre-VAT) -->

In the "Purchase type" column specify wheater this is

- **CC** A direct purchase from a Centralized Contract Lot
  [list](https://www.nuigalway.ie/procurement-contracts/centralisedcontracts/).
  A quote may be necessary.
- **SS** A purchase from a sole source (unique supplier)
- **1Q** A purchase worth less than €5000 outside the centralized contracts.
  One quote **must** be attached.
- **3Q** A purchase worth _more_ than €5000 outside the centralized contracts.
  Three quotes **must** be attached.

In "Tax code" column specify either:

 - **P2** for Irish taxation
 - **E2** for European taxation
 - **I2** for other international taxation

The "Status" column can have the following values:

- "" (*blank*): not processed
- **POR**: Purchase order is submitted to Agresso
- **PO**: Purchase order is submitted to the supplier
- **GRN**: The item was received and "GRN'd"
- *Issue*: A string specifying the current issue.

| Supplier | CAT Number | Product description | Units | Unit price | Tax code | Purchase type | Status |
|----------|------------|---------------------|-------|------------|----------|---------------|--------|
|  |  |  |  |  |  |  |  |

## Quote and other attachment
<!-- Attach quotes or any other item relevant to the requisition. -->
