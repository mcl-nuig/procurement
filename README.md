<!--
Here the markdown "reference style" is used.  Hence, reference links are
enclosed in square brackets [ ] instead of parenthesis ( ).
-->

<!-- README header -->
<br />
<div align="center">
  <a href="https://gitlab.com/gcl-nuig/procurement">
    <img src="data/.attachments/gcl-logo.png" alt="Logo" width="145" height="120">
  </a>

  <h3 align="center">Procurement</h3>
  <p align="center">
    Gavin Collins Laboratory Group procurement resources.
    <br />
    <br />
    <a href="https://gitlab.com/gcl-nuig/procurement/-/issues/new"><strong>Create a requisition! </strong></a><a href="mailto:incoming+gcl-nuig-procurement-26027862-issue-@incoming.gitlab.com">(mobile)</a>
    <br />
    <br />
    or
    <a href="https://gitlab.com/gcl-nuig/procurement/-/issues">Explore previous requisitions</a>
  </p>
</div>

<!-- TABLE OF CONTENTS -->
[[_TOC_]]

<!-- PROCUREMENT VOCABULARY -->
## Procurement vocabulary

 - **Requisition:** A request for an order to be made.  This task can be carried
 out by any person in the lab by creating an Issue on this GitLab repository
 using the requisition template.

 - **Issue:** A threaded discussion to (_i_) discuss an order or other topic;
 (_ii_) track tasks and work status; and (_iii_) elaborate on workflow
 improvements and implementations.  See the GitLab
 [documentation](https://docs.gitlab.com/ee/user/project/issues/) on how issues
 work.

 - **Procurement order requisition (POR):** The official order requisition sent
 to <microbiologyorders@nuigalway.ie> for processing.  PORs must be written
 using the [POR template](/POR-template.xlsx) without any modification.  That
 is, the POR must respect the template format.  When a POR is completed, this
 should be attached to the relevant requisition (issue).

 - **Tax code (T):** Each ordered item is subject to the value-added tax (VAT)
 applied by the Irish government and paid by us.  When placing a POR, one of the
 three different VAT codes (**P**, **U**, or **I**) must be specified. **P** is
 used for *Irish* goods, **U** for *European* goods, and **I** for
 *extra-Europeans* (or international) goods.

 - **Quote:** An official price estimate provided by the goods supplier.  For
 any order placed to suppliers other than Fisher and Sigma, one (< €5000) or
 three quotes (> €5000) are required.  A quote must be attached to the issue and
 sent to Vidoja along the relative POR.

 - **Procurement order (PO):** The output of a processed POR.  This is managed
 by the procurement admin (see below).

 - **Sub Account (Subacc):** The account (*budget*) from which an item is paid
 from.  Note that the an item is actually paid off one of the (main) NUIG
 accounts, each of which is dedicated to a [procurement
 category](/documentation/Product-code-ONLINE.pdf) (e.g., laboratory consumables
 are paid off the `3136` account).  When the ordered item is received the used
 main account is replenish from the selected subacc.

 - **Procurement admin:** A person in charge of processing order requests on
 Agresso.  This is currently (2022-05-03) Vidoja Kovacevic
 <microbiologyorders@nuigalway.ie>.

 - **Budget holder:** The person with complete access level to a subacc.  A
 budget holder is the only person able to grant access to a subacc.  Different
 level of access may be specified.  Best practices suggests that procurement
 admins are granted with *€500 maximum spending* access.  Hence, POR over the
 specified access level will be routed to the budget holder for approval.

 - **Agresso:** The NUIG procurement electronic system located at
 <https://agresso.nuigalway.ie/agresso/login/login.aspx>.  Only procurement
 admin(s) have access to Agresso, which may be granted by ISS upon request from
 the group super-admin.

## The procurement workflow

1. Identify what you need and who sells it for the best price.
2. Look on the GitLab issue list if an issue for the same company, labeled with
   "POR" or "WAIT:Requisition" exists. If such issue exists, comment the issue
   asking to add your desired items. Else, if such issue does NOT exists,
   create a new issue following the guide in the template.
3. Once the issue is reviewed, attach the POR and needed quote.  Every 14 days
   all issues will be processed and the PORs (and quotes) sent to
   <microbiologyorders@nuigalway.ie>.  Such issues are labeled "WAIT:Vidoja".
4. Once Vidoja have confirmed the POR processing the issue is labeled
   "WAIT:delivery".
5. When all items ordered are received, the relative issue can be labeled
   "DONE" and closed.

Additionally label are used to classify issues.  A list is available at https://gitlab.com/gcl-nuig/procurement/-/labels

## Tips and tricks

### Order size

PORs should be small (10 items or less).  When a PO is linked to an invoice,
the invoice can be paid only if all the items on the PO are received (GRN'd).
For this reason, small PORs, thus small POs, limit big invoices to be locked
by the delayed delivery of a single (or few) item.

### Suppliers that do not require a quote

Certain suppliers have a e-shop for which Vidoja has contractual prices.
For such suppliers a quote is not needed.

These are (list subject to alteration):

- Sparks Lab Supplies
- Cruinn Diagnostics
- Lennox

### Minimum order size for specific items

Some items have a minimum order size.  This might change and you should confirm
the minimum order size with the supplier or Vidoja (when a quote is not needed).
The list is updated as we learn more.

| item | minimum order size |
| ------ | ------ |
| Gloves box | 10 boxes |
| 0.5-5.0 ml centrifuge tubes from Cruinn | 1000 pieces (generally 10 bags) |

## Useful resources

- [NUIG procurement
  guide](https://www.nuigalway.ie/procurement-contracts/stepbystepguide/)
- [Centralized
  contracts](https://www.nuigalway.ie/procurement-contracts/centralisedcontracts/)
  to use for "CC" orders.
- [Quote request template](https://www.nuigalway.ie/procurement-contracts/templatesforms/)
- [GitLab documentation for issues and related
  operations](https://docs.gitlab.com/ee/topics/plan_and_track.html)
- [Agresso product codes](./documentation/Product-code-ONLINE.pdf)

## Contact

- Marco Prevedello    <m.prevedello1@nuigalway.ie>    +353 (85) 256 6133
- Gavin Collins       <gavin.collins@nuigalway.ie>    +353 (86) 304 2324
- Vidoja Kovacevic    <vidoja.kovacevic@nuigalway.ie> (Internal phone) 4417

## License

This project is private.  All text, source code, and other files in this
repository as under exclusive copyright.
